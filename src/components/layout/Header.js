import React from 'react';

const Header = () => {
  return (
    <header>
      <h1>
        <a href="/">Дискотека</a>
      </h1>
    </header>
  );
};

export default Header;
